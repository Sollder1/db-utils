package de.sollder1.dbutils.jakartaext;

import de.sollder1.dbutils.core.query.mapper.object.ObjectInspector;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ExtensibilityTest {

    @Test
    public void testExtensibility() {
        Assertions.assertEquals(2, ObjectInspector.getColumnNameResolver().size());
    }

}
