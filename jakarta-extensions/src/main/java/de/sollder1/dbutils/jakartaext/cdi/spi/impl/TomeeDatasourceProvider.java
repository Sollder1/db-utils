package de.sollder1.dbutils.jakartaext.cdi.spi.impl;

import de.sollder1.dbutils.jakartaext.cdi.spi.DatasourceProvider;
import jakarta.inject.Singleton;

import javax.naming.InitialContext;
import javax.sql.DataSource;

@Singleton
public class TomeeDatasourceProvider implements DatasourceProvider {

    @Override
    public DataSource provide(String datasourceName) throws Exception {
        //Hint: Datasource-Ressource-Entry from tomee.xml
        return (DataSource) new InitialContext().lookup("openejb:Resource/%s".formatted(datasourceName));
    }

    @Override
    public int priority() {
        return 1;
    }


}
