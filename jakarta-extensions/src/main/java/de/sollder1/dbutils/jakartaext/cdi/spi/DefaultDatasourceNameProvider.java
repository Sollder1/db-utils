package de.sollder1.dbutils.jakartaext.cdi.spi;

/**
 * Return the name of the {@link javax.sql.DataSource} if no JdbcUtilsContext is added to the Inejction-Point.
 */
public interface DefaultDatasourceNameProvider {
    /**
     * @return The name to be used in default cases.
     */
    String provide();

}
