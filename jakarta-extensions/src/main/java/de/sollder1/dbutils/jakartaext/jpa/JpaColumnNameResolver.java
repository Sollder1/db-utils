package de.sollder1.dbutils.jakartaext.jpa;

import de.sollder1.dbutils.core.query.mapper.object.spi.CustomExpectedColumnNameResolver;
import jakarta.persistence.Column;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Optional;

/**
 * Makes the {@link de.sollder1.dbutils.core.query.mapper.object.ObjectMapper} aware of JPA-Columns.
 */
public class JpaColumnNameResolver implements CustomExpectedColumnNameResolver {


    @Override
    public Optional<String> determineColumnName(Method getter, Method setter, Field field) {
        Column fieldAnnotation = field.getAnnotation(Column.class);
        if (fieldAnnotation != null) {
            return fieldAnnotation.name().describeConstable();
        }

        Column setterAnnotation = setter.getAnnotation(Column.class);
        if (setterAnnotation != null) {
            return setterAnnotation.name().describeConstable();
        }

        Column getterAnnotation = getter.getAnnotation(Column.class);
        if (getterAnnotation != null) {
            return getterAnnotation.name().describeConstable();
        }
        return Optional.empty();
    }

    @Override
    public Optional<String> determineColumnName(Parameter parameter) {
        var annotation = parameter.getAnnotation(Column.class);
        if (annotation != null) {
            return annotation.name().describeConstable();
        }

        return Optional.empty();
    }

    @Override
    public int priority() {
        return 100;
    }

    @Override
    public Class<? extends Annotation> getAnnotationType() {
        return Column.class;
    }
}
