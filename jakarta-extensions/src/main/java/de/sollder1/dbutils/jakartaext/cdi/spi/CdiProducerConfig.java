package de.sollder1.dbutils.jakartaext.cdi.spi;

/**
 * One may provide 0..1 Implementations of this interface.
 * Allows to customize the CdiProducer used to create and manage JdbcUtils-Instances as Cdi-Beans.
 */
public interface CdiProducerConfig {

    /**
     * @return if a per Datasource-name instance-cache should be used.
     */
    boolean cacheable();

}
