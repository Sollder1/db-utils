package de.sollder1.dbutils.jakartaext.cdi;

import de.sollder1.dbutils.core.JdbcUtils;
import de.sollder1.dbutils.jakartaext.cdi.annotations.JdbcUtilsContext;
import de.sollder1.dbutils.jakartaext.cdi.spi.CdiProducerConfig;
import de.sollder1.dbutils.jakartaext.cdi.spi.DatasourceProvider;
import de.sollder1.dbutils.jakartaext.cdi.spi.DefaultDatasourceNameProvider;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.inject.Instance;
import jakarta.enterprise.inject.Produces;
import jakarta.enterprise.inject.spi.Annotated;
import jakarta.enterprise.inject.spi.InjectionPoint;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import lombok.extern.slf4j.Slf4j;

import javax.sql.DataSource;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Allows to {@link Inject} JdbcUtils.
 * Defaults are highly Implementation-Specific, but one can shape the entire Discovery-Process by using the provided SPIs.
 * These spi are loaded with CDI instead of the ServiceLoader, because this is a jakartaEE-extension.
 */
@Slf4j
@Singleton
public class CdiProducer {

    @Inject
    private Instance<DatasourceProvider> customProvider;
    @Inject
    private Instance<DefaultDatasourceNameProvider> datasourceNameProviders;
    @Inject
    private Instance<CdiProducerConfig> cdiProducerConfigs;

    private boolean useCache = true;

    private final Map<String, JdbcUtils> INSTANCES = new ConcurrentHashMap<>();

    @PostConstruct
    private void init() {
        if (cdiProducerConfigs.isAmbiguous()) {
            throw new IllegalStateException("CdiProducerConfig ambiguous");
        }

        if (!cdiProducerConfigs.isUnsatisfied()) {
            useCache = cdiProducerConfigs.get().cacheable();
        }
    }

    @Produces
    public JdbcUtils produce(InjectionPoint ip) {
        Annotated annotated = ip.getAnnotated();
        String datasourceName = produceDatasourceName(annotated);

        if (useCache) {
            return INSTANCES.computeIfAbsent(datasourceName, key -> new JdbcUtils(produceDatasource(key)));
        } else {
            return new JdbcUtils(produceDatasource(datasourceName));
        }
    }

    private String produceDatasourceName(Annotated annotated) {
        if (annotated.isAnnotationPresent(JdbcUtilsContext.class)) {
            JdbcUtilsContext jdbcUtilsContext = annotated.getAnnotation(JdbcUtilsContext.class);
            return jdbcUtilsContext.dataSourceName();
        }

        if (datasourceNameProviders.isResolvable()) {
            return datasourceNameProviders.get().provide();
        }

        return "default";
    }

    private DataSource produceDatasource(String datasourceName) {

        List<DatasourceProvider> datasourceProviders = customProvider.stream()
                .sorted(Comparator.comparing(DatasourceProvider::priority).reversed())
                .toList();

        for (var datasourceProvider : datasourceProviders) {
            try {
                return datasourceProvider.provide(datasourceName);
            } catch (Exception e) {
                log.info("Could not use {} for datasource-name {}, trying next if any...", datasourceProvider.getClass(), datasourceName, e);
            }
        }

        throw new IllegalArgumentException("""
                No Datasource-Provider was abel to create Datasource out of name '%s'.
                You may need to provide a custom one yourself, as we by default only support 'tomee'.
                """.formatted(datasourceName));
    }

}