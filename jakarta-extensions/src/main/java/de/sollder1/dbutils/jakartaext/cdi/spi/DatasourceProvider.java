package de.sollder1.dbutils.jakartaext.cdi.spi;

import javax.sql.DataSource;

/**
 * Defines the way to map a datasourceName to an actual {@link DataSource}, which is NOT AT ALL straight forward,
 * portable or specified in the JakartaEE specs. So have fun here.
 * <p>
 * Because it is the only Appserver I truly love an implementation for tomee is already provided
 * ({@link de.sollder1.dbutils.jakartaext.cdi.spi.impl.TomeeDatasourceProvider}).
 * <p>
 * Other impls are of course always welcome.
 */
public interface DatasourceProvider {

    /**
     * @param datasourceName The Datasource to provide.
     * @return The requested Datasource
     * @throws Exception If this DatasourceProvider can for some reason not provide a Datasource
     *                   for the given datasourceName. Example: We start the app in Wildfly and the
     *                   {@link de.sollder1.dbutils.jakartaext.cdi.spi.impl.TomeeDatasourceProvider}
     *                   has a higher priority it will throw an exception to let the next prioritized PProvider try it.
     */
    DataSource provide(String datasourceName) throws Exception;

    /**
     * @return The priority ofg applying this specific DatasourceProvider.
     */
    int priority();

}
