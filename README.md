# db-utils

A simple, yet powerful tool to Work with jdbc queries more easily.

## Goals

- [x] Query data with a simple API using prepared Statements
- [x] Automap data from sql queries to Java-Beans according to the java - Beans Spec
- [x] Automap data from sql queries to Java-Records with only one constructor
- [x] Support for Jakarta Persistence API Features without requiring them. There as the need for a plugin Architectures
  of some kind.
- [x] Support for Jakarta Contexts and Dependency Injection Features.
- [x] Support for ObjectColumn.java Annotation, that allows to Map parts of a result to Subobjects with Java-Beans.
- [x] Support for ObjectColumn.java Annotation, that allows to Map parts of a result to Java-Records with only one
  constructor.
- [x] Support for Type-Proxies to allow conversion of types a driver might not directly support (like Enums). 