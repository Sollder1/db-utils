package de.sollder1.dbutils.core;

import de.sollder1.dbutils.core.query.mapper.GenericArrayMapper;
import de.sollder1.dbutils.core.query.select.NamedSelectQuery;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;

public class NamedSelectQueryTests extends H2EnabledTest {


    @BeforeAll
    public static void init() {
        DataSource datasource = createDatasource();
        jdbcUtils = new JdbcUtils(datasource);
        jdbcUtils.execute("""
                CREATE TABLE person(
                      id VARCHAR(128) PRIMARY KEY,
                      username varchar(128) NOT NULL
                );
                """);
        jdbcUtils.runUpdate("INSERT INTO person (id, username) VALUES (?, ?)", "1", "sollder1");
    }

    @Test
    public void testParamOrder() {

        var query = NamedSelectQuery.builder(GenericArrayMapper.INSTANCE)
                .sql("select * from person WHERE username = :u_name AND id = :id;")
                .param("u_name", "s")
                .param("id", "1")
                .build();

        var objects = query.provideParams();

        Assertions.assertEquals("s", objects.get(0));
        Assertions.assertEquals("1", objects.get(1));
    }

    @Test
    public void testNotProvidesParamFails() {

        var query = NamedSelectQuery.builder(GenericArrayMapper.INSTANCE)
                .sql("select * from person WHERE person = :A AND Y = :JJ;")
                .param("JJ", false)
                .build();


        Assertions.assertThrows(IllegalArgumentException.class, query::provideParams);
    }


    @Test
    public void testSqlGenerated() {

        var query = NamedSelectQuery.builder(GenericArrayMapper.INSTANCE)
                .sql("select * from person WHERE person = :A AND Y = :JJ;")
                .param("JJ", false)
                .param("A", 3)
                .build();
        Assertions.assertEquals("select * from person WHERE person = ? AND Y = ?;", query.provideSql());

    }

    @Test
    public void testActuallyRuns() {
        var result = NamedSelectQuery.builderSingleColumn(String.class)
                .sql("select username from person WHERE username = :u_name OR id = :id;")
                .param("u_name", "sso")
                .param("id", "1")
                .querySingular(jdbcUtils);
        Assertions.assertEquals("sollder1", result);

    }
}
