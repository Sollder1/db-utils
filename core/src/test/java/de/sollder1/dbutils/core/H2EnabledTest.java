package de.sollder1.dbutils.core;

import org.h2.jdbcx.JdbcDataSource;

import javax.sql.DataSource;

public abstract class H2EnabledTest {

    protected static JdbcUtils jdbcUtils;


    protected static DataSource createDatasource() {
        var dataSource = new JdbcDataSource();
        dataSource.setUser("postgres");
        dataSource.setPassword("postgres");
        dataSource.setUrl("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1");
        return dataSource;
    }

}
