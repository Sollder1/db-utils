package de.sollder1.dbutils.core;

import de.sollder1.dbutils.core.query.mapper.GenericListMapper;
import de.sollder1.dbutils.core.query.mapper.GenericMapMapper;
import de.sollder1.dbutils.core.query.mapper.object.Column;
import de.sollder1.dbutils.core.query.mapper.object.ObjectMapper;
import de.sollder1.dbutils.core.query.select.IndexedSelectQuery;
import lombok.Getter;
import lombok.Setter;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BasicTests extends H2EnabledTest {

    @BeforeAll
    public static void init() {
        DataSource datasource = createDatasource();
        jdbcUtils = new JdbcUtils(datasource);
        jdbcUtils.execute("""
                CREATE TABLE test(
                      id VARCHAR(128) PRIMARY KEY
                );
                """);
    }

    @Test
    public void insert() throws SQLException {
        assertEquals(1, simpleInsert("Test1"));
    }


    @Test
    public void testSimpleSelect_List() throws SQLException {
        simpleInsert("Test2");
        var queryBuilder = new IndexedSelectQuery<>("SELECT * FROM test WHERE id = ?;", GenericListMapper.INSTANCE, "Test2");
        var list = jdbcUtils.query(queryBuilder);
        assertEquals(1, list.size());
        assertEquals(1, list.getFirst().size());
        assertEquals("Test2", list.getFirst().getFirst());
    }


    @Test
    public void testSimpleSelect_Map() throws SQLException {
        simpleInsert("Test3");
        var queryBuilder = new IndexedSelectQuery<>("SELECT * FROM test WHERE id = ?;", GenericMapMapper.INSTANCE, "Test3");
        var map = jdbcUtils.query(queryBuilder);
        assertEquals(1, map.size());
        assertEquals(1, map.get(0).size());
        assertEquals("Test3", map.get(0).get("ID"));
    }


    @Test
    public void testSimpleSelect_Bean() throws SQLException {
        simpleInsert("Test4");
        var result = IndexedSelectQuery.builder(MapppingTarget.class)
                .sql("SELECT * FROM test WHERE id = ?;")
                .param("Test4")
                .query(jdbcUtils);
        assertEquals(1, result.size());
        assertEquals("Test4", result.getFirst().getId__());
    }

    @Test
    public void testSimpleSelect_Record() throws SQLException {
        simpleInsert("Test5");
        var queryBuilder = new IndexedSelectQuery<>("SELECT * FROM test WHERE id = ?;", new ObjectMapper<>(MapppingTargetRecord.class), "Test5");
        var result = jdbcUtils.query(queryBuilder);
        assertEquals(1, result.size());
        assertEquals("Test5", result.get(0).id__());
    }


    @Test
    public void testNoParamsProvided() throws SQLException {
        //HInt: We only want to see that no Exception is thrown
        var query = IndexedSelectQuery.builder(MapppingTargetRecord.class)
                .sql("SELECT * FROM test;")
                .query(jdbcUtils);
    }

    private static int simpleInsert(String id) throws SQLException {
        return jdbcUtils.runUpdate("INSERT INTO test(id) VALUES (?)", id);
    }


    @Getter
    @Setter
    public static class MapppingTarget {
        @Column("id")
        private String id__;
    }

    public record MapppingTargetRecord(@Column("id") String id__) {

    }

}
