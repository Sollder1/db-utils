package de.sollder1.dbutils.core;

import de.sollder1.dbutils.core.query.select.IndexedSelectQuery;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;

public class SingularColumnTest extends H2EnabledTest {

    public static final int BOOK_COUNT = 5;

    @BeforeAll
    public static void init() {

        DataSource datasource = createDatasource();
        jdbcUtils = new JdbcUtils(datasource);

        jdbcUtils.execute("""
                   CREATE TABLE test_sing(
                      id VARCHAR(128) PRIMARY KEY,
                      pages int NOT NULL
                );
                """);
        for (int i = 0; i < BOOK_COUNT; i++) {
            jdbcUtils.runUpdate("INSERT INTO test_sing (id, pages) VALUES (?, ?)", i + "", 5);
        }

    }

    @Test
    public void selectString() {
        String sql = """
                SELECT id FROM test_sing;
                """;
        var res = IndexedSelectQuery.builderSingleColumn(String.class)
                .sql(sql)
                .query(jdbcUtils);
        Assertions.assertEquals(BOOK_COUNT, res.size());
    }

    @Test
    public void selectInteger() {
        String sql = """
                SELECT pages FROM test_sing;
                """;
        var res = IndexedSelectQuery.builderSingleColumn(Integer.class)
                .sql(sql)
                .query(jdbcUtils);
        Assertions.assertEquals(BOOK_COUNT, res.size());
    }


    @Test
    public void selectIntPrimitive() {
        String sql = """
                SELECT pages FROM test_sing;
                """;
        var res = IndexedSelectQuery.builderSingleColumn(int.class)
                .sql(sql)
                .query(jdbcUtils);
        Assertions.assertEquals(BOOK_COUNT, res.size());
    }
}