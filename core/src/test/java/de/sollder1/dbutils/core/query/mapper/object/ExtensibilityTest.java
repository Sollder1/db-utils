package de.sollder1.dbutils.core.query.mapper.object;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ExtensibilityTest {

    @Test
    public void testExtensibility() {
        Assertions.assertEquals(1, ObjectInspector.getColumnNameResolver().size());
    }

}

