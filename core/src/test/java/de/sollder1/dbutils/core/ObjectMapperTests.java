package de.sollder1.dbutils.core;

import de.sollder1.dbutils.core.query.exceptions.MultiValueResultException;
import de.sollder1.dbutils.core.query.exceptions.SqlExceptionRuntimeWrapper;
import de.sollder1.dbutils.core.query.mapper.object.BeanSettings;
import de.sollder1.dbutils.core.query.mapper.object.Column;
import de.sollder1.dbutils.core.query.mapper.object.ObjectColumn;
import de.sollder1.dbutils.core.query.mapper.object.ObjectMapper;
import de.sollder1.dbutils.core.query.mapper.object.exceptions.InspectorException;
import de.sollder1.dbutils.core.query.select.IndexedSelectQuery;
import lombok.Getter;
import lombok.Setter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;
import java.security.SecureRandom;

public class ObjectMapperTests extends H2EnabledTest {

    public static final int PUB_COUNT = 10;
    public static final int BOOK_COUNT = 1000;

    @BeforeAll
    public static void init() {

        DataSource datasource = createDatasource();
        jdbcUtils = new JdbcUtils(datasource);
        jdbcUtils.execute("""
                   CREATE TABLE publisher(
                      id VARCHAR(128) PRIMARY KEY,
                      name VARCHAR(128) NOT NULL,
                      type VARCHAR(128)
                );
                """);
        jdbcUtils.execute("""
                       CREATE TABLE book(
                          id VARCHAR(128) PRIMARY KEY,
                          titel VARCHAR(128) NOT NULL,
                          pages int NOT NULL,
                          publisher_id VARCHAR(128) references publisher(id)
                    );
                """);
        for (int i = 0; i < PUB_COUNT; i++) {
            jdbcUtils.runUpdate("INSERT INTO publisher (id, name, type) VALUES (?, ?, ?)",
                    i + "", "Pub %s".formatted(i), "A");
        }

        SecureRandom random = new SecureRandom();
        for (int i = 0; i < BOOK_COUNT; i++) {
            jdbcUtils.runUpdate("INSERT INTO book (id, titel, pages, publisher_id) VALUES (?, ?, ?, ?)", i + "",
                    "Book %s".formatted(i), random.nextInt(2000), i % PUB_COUNT);
        }

    }

    @Test
    public void records_nameMappings() {
        String sql = """
                SELECT b.publisher_id as pub_id, COUNT(*) as book_count FROM book b GROUP BY b.publisher_id;
                """;
        var results = jdbcUtils.query(new IndexedSelectQuery<>(sql, new ObjectMapper<>(BooksByPublisher.class)));
        Assertions.assertEquals(PUB_COUNT, results.size());
        for (BooksByPublisher result : results) {
            Assertions.assertNotNull(result.count());
            Assertions.assertEquals(BOOK_COUNT / PUB_COUNT, result.count());
            Assertions.assertNotNull(result.publisherId());
        }
    }

    @Test
    public void records_unknownSelect_Ok() {
        String sql = """
                SELECT b.publisher_id as pub_id, COUNT(*) as book_count, AVG(pages) FROM book b GROUP BY b.publisher_id;
                """;
        var results = jdbcUtils.query(new IndexedSelectQuery<>(sql, new ObjectMapper<>(BooksByPublisher.class)));
        Assertions.assertEquals(PUB_COUNT, results.size());
        for (BooksByPublisher result : results) {
            Assertions.assertNotNull(result.count());
            Assertions.assertEquals(BOOK_COUNT / PUB_COUNT, result.count());
            Assertions.assertNotNull(result.publisherId());
        }
    }

    @Test
    public void records_unknownField_NotOk() {
        Assertions.assertThrows(SqlExceptionRuntimeWrapper.class, () -> {
            String sql = """
                    SELECT b.publisher_id as pub_id FROM book b GROUP BY b.publisher_id;
                    """;
            jdbcUtils.query(new IndexedSelectQuery<>(sql, new ObjectMapper<>(BooksByPublisher.class)));
        });
    }

    @Test
    public void records_multiConstructor_notOk() {
        Assertions.assertThrows(InspectorException.class, () -> {
            String sql = """
                    SELECT b.publisher_id as pub_id FROM book b GROUP BY b.publisher_id;
                    """;
            jdbcUtils.query(new IndexedSelectQuery<>(sql, new ObjectMapper<>(BooksByPublisherMultiConstructor.class)));
        });
    }


    @Test
    public void records_singular_moreThanOne_throws() {
        Assertions.assertThrows(MultiValueResultException.class, () -> {
            String sql = """
                    SELECT b.publisher_id as pub_id, COUNT(*) as book_count FROM book b GROUP BY b.publisher_id;
                    """;
            jdbcUtils.querySingular(new IndexedSelectQuery<>(sql, new ObjectMapper<>(BooksByPublisher.class)));
        });
    }

    @Test
    public void records_singular_none_throws() {
        Assertions.assertThrows(MultiValueResultException.class, () -> {
            String sql = """
                    SELECT b.publisher_id as pub_id, 1 FROM BOOK b WHERE id = 'blub';
                    """;
            jdbcUtils.querySingular(new IndexedSelectQuery<>(sql, new ObjectMapper<>(BooksByPublisher.class)));
        });
    }

    @Test
    public void records_singular_exactly_one() {
        String sql = """
                SELECT b.publisher_id as pub_id, COUNT(*) as book_count FROM book b GROUP BY b.publisher_id LIMIT 1;
                """;
        var res = jdbcUtils.querySingular(new IndexedSelectQuery<>(sql, new ObjectMapper<>(BooksByPublisher.class)));
        Assertions.assertNotNull(res);
        Assertions.assertEquals(BOOK_COUNT / PUB_COUNT, (long) res.count());
    }


    @Test
    public void records_optional_moreThanOne_throws() {
        Assertions.assertThrows(MultiValueResultException.class, () -> {
            String sql = """
                    SELECT b.publisher_id as pub_id, COUNT(*) as book_count FROM book b GROUP BY b.publisher_id;
                    """;
            jdbcUtils.queryOptional(new IndexedSelectQuery<>(sql, new ObjectMapper<>(BooksByPublisher.class)));
        });
    }

    @Test
    public void records_optional_none() {
        String sql = """
                SELECT b.publisher_id as pub_id, 1 FROM BOOK b WHERE id = ?;
                """;
        var res = jdbcUtils.queryOptional(new IndexedSelectQuery<>(sql, new ObjectMapper<>(BooksByPublisher.class), "foo"));
        Assertions.assertTrue(res.isEmpty());
    }

    @Test
    public void records_optional_exactly_one() {
        String sql = """
                SELECT b.publisher_id as pub_id, COUNT(*) as book_count FROM book b GROUP BY b.publisher_id LIMIT 1;
                """;
        var res = jdbcUtils.queryOptional(new IndexedSelectQuery<>(sql, new ObjectMapper<>(BooksByPublisher.class)));
        Assertions.assertTrue(res.isPresent());
        Assertions.assertEquals(BOOK_COUNT / PUB_COUNT, (long) res.get().count());
    }

    @Test
    public void bean_inheritance() {
        String sql = """
                SELECT id, titel FROM book b LIMIT 1;
                """;
        var res = jdbcUtils.querySingular(new IndexedSelectQuery<>(sql, new ObjectMapper<>(InheritanceTest.class)));
        Assertions.assertNotNull(res.getId());
        Assertions.assertNotNull(res.getBookTitle());
    }


    @Test
    public void bean_annotations_allWork() {
        String sql = """
                SELECT id, titel, pages, publisher_id FROM book b LIMIT 1;
                """;
        var res = jdbcUtils.querySingular(new IndexedSelectQuery<>(sql, new ObjectMapper<>(AllAnnotationsTest.class)));
        Assertions.assertNotNull(res.getId());
        Assertions.assertNotNull(res.getBookTitle());
        Assertions.assertNotNull(res.getBookPages());
        Assertions.assertNotNull(res.getPublisherId());
    }

    @Test
    public void bean_annotations_presidency() {
        String sql = """
                SELECT id, titel, pages, publisher_id FROM book b LIMIT 1;
                """;
        var res = jdbcUtils.querySingular(new IndexedSelectQuery<>(sql, new ObjectMapper<>(AllAnnotationsTest.class)));
        Assertions.assertNotNull(res.getId());
        Assertions.assertNotNull(res.getBookTitle());
        Assertions.assertNotNull(res.getBookPages());
        Assertions.assertNotNull(res.getPublisherId());
    }

    //Hint: Utterly ridiculous structure to test deeper recursion
    @Test
    public void bean_objectColums() {
        String sql = """
                SELECT b.id, b.titel, b.pages AS "p.pages", p.id AS "publisher.id", p.name AS "publisher.name.name" 
                FROM book b LEFT JOIN publisher p ON b.publisher_id = p.id LIMIT 1;
                """;
        var res = jdbcUtils.querySingular(new IndexedSelectQuery<>(sql, new ObjectMapper<>(Book.class)));
        Assertions.assertNotNull(res.getId());
        Assertions.assertNotNull(res.getSeiten());
        Assertions.assertNotNull(res.getSeiten().getPages());
        Assertions.assertNotNull(res.getTitle());
        Assertions.assertNotNull(res.getPublisher());
        Assertions.assertNotNull(res.getPublisher().getId());
        Assertions.assertNotNull(res.getPublisher().getName_());
        Assertions.assertNotNull(res.getPublisher().getName_().getName_());
    }

    @Test
    public void bean_objectColum_wrong() {
        String sql = """
                SELECT b.id, b.titel, b.pages AS "p.pages", p.id AS "publisher.id", p.name AS "publisher.name.name" 
                FROM book b LEFT JOIN publisher p ON b.publisher_id = p.id LIMIT 1;
                """;
        Assertions.assertThrows(SqlExceptionRuntimeWrapper.class, () -> {
            jdbcUtils.querySingular(new IndexedSelectQuery<>(sql, new ObjectMapper<>(IncorrectlyDeclaredBook.class)));
        });
    }

    @Test
    public void bean_objectColum_mixed() {
        String sql = """
                SELECT b.id, b.titel, b.pages AS "p.pages", p.id AS "publisher.id", p.name AS "publisher.name.name" 
                FROM book b LEFT JOIN publisher p ON b.publisher_id = p.id LIMIT 1;
                """;
        Assertions.assertThrows(InspectorException.class, () -> {
            jdbcUtils.querySingular(new IndexedSelectQuery<>(sql, new ObjectMapper<>(MixedBean.class)));
        });
    }


    @Test
    public void record_objectColums() {
        String sql = """
                SELECT b.id, b.titel, b.pages AS "p.pages", p.id AS "publisher.id", p.name AS "publisher.name.name" 
                FROM book b LEFT JOIN publisher p ON b.publisher_id = p.id LIMIT 1;
                """;
        var res = jdbcUtils.querySingular(new IndexedSelectQuery<>(sql, new ObjectMapper<>(BookRecord.class)));
        Assertions.assertNotNull(res.id());
        Assertions.assertNotNull(res.seiten());
        Assertions.assertNotNull(res.seiten().pages());
        Assertions.assertNotNull(res.seiten());
        Assertions.assertNotNull(res.publisher());
        Assertions.assertNotNull(res.publisher().id());
        Assertions.assertNotNull(res.publisher().name_());
        Assertions.assertNotNull(res.publisher().name_().name_());
    }

    @Test
    public void record_objectColum_wrong() {
        String sql = """
                SELECT b.id, b.titel, b.pages AS "p.pages", p.id AS "publisher.id", p.name AS "publisher.name.name" 
                FROM book b LEFT JOIN publisher p ON b.publisher_id = p.id LIMIT 1;
                """;
        Assertions.assertThrows(SqlExceptionRuntimeWrapper.class, () -> {
            jdbcUtils.querySingular(new IndexedSelectQuery<>(sql, new ObjectMapper<>(IncorrectlyDeclaredBookRecord.class)));
        });
    }

    @Test
    public void record_objectColum_mixed() {
        String sql = """
                SELECT b.id, b.titel, b.pages AS "p.pages", p.id AS "publisher.id", p.name AS "publisher.name.name" 
                FROM book b LEFT JOIN publisher p ON b.publisher_id = p.id LIMIT 1;
                """;
        Assertions.assertThrows(InspectorException.class, () -> {
            jdbcUtils.querySingular(new IndexedSelectQuery<>(sql, new ObjectMapper<>(MixedRecord.class)));
        });
    }


    @Test
    public void record_objectColum_enum() {
        String sql = """
                SELECT type FROM publisher LIMIT 1;
                """;
        var result = jdbcUtils.querySingular(new IndexedSelectQuery<>(sql, new ObjectMapper<>(Enum.class)));
        Assertions.assertNotNull(result);
        Assertions.assertEquals(Type.A, result.en());
    }


    @Test
    public void strict_test() {
        String sql = """
                SELECT type FROM publisher LIMIT 1;
                """;
        var result = jdbcUtils.querySingular(new IndexedSelectQuery<>(sql, new ObjectMapper<>(StrictTest.class)));
        Assertions.assertNotNull(result);
        //Hint: Actually could be not null
        Assertions.assertNull(result.getType());
        //Hint: Actually would create an error
        Assertions.assertNull(result.getGarbadge());
        //Hint: Actually would create an error
        Assertions.assertNotNull(result.getTypeAnnotated());

    }


    @Getter
    @Setter
    @BeanSettings(onlyMapAnnotatedFields = true)
    public static class StrictTest {
        Type type;
        String garbadge;
        @Column("type")
        Type typeAnnotated;

    }


    public record Enum(@Column("type") Type en) {

    }


    public record BooksByPublisher(@Column("pub_id") String publisherId, @Column("book_count") Long count) {

    }

    public record BooksByPublisherMultiConstructor(@Column("pub_id") String publisherId,
                                                   @Column("book_count") Long count) {
        public BooksByPublisherMultiConstructor(Long count) {
            this("-", count);
        }
    }


    @Getter
    @Setter
    public static class InheritanceTest extends InheritanceTestBase {
        @Column("titel")
        private String bookTitle;
    }

    @Getter
    @Setter
    public static abstract class InheritanceTestBase {
        private String id;
    }

    @Getter
    @Setter
    public static class AllAnnotationsTest {
        private String id;
        private String bookTitle;
        private Integer bookPages;
        @Column("publisher_id")
        private String publisherId;

        @Column("titel")
        public String getBookTitle() {
            return bookTitle;
        }

        @Column("pages")
        public void setBookPages(Integer bookPages) {
            this.bookPages = bookPages;
        }
    }

    @Getter
    @Setter
    public static class AnnotationPresidencyTest {
        private String id;
        private String bookTitle;
        @Column("pages")
        private Integer bookPages;
        @Column("publisher_id")
        private String publisherId;

        @Column("titel")
        public void setBookTitle(String bookTitle) {
            this.bookTitle = bookTitle;
        }

        @Column("titel_______________________")
        public String getBookTitle() {
            return bookTitle;
        }

        @Column("pages___")
        public Integer getBookPages() {
            return bookPages;
        }

        @Column("pages_____")
        public void setBookPages(Integer bookPages) {
            this.bookPages = bookPages;
        }
    }

    @Getter
    @Setter
    public static class Book {
        private String id;
        @Column("titel")
        private String title;
        @ObjectColumn(prefix = "p")
        private Pages seiten;
        @ObjectColumn(prefix = "publisher")
        private Publisher publisher;
    }

    @Getter
    @Setter
    public static class Pages {
        private String pages;
    }


    @Getter
    @Setter
    public static class Publisher {
        private String id;
        @ObjectColumn(prefix = "name")
        private PublisherName name_;
    }

    @Getter
    @Setter
    public static class PublisherName {
        @Column("name")
        private String name_;
    }

    @Getter
    @Setter
    public static class IncorrectlyDeclaredBook {
        private String id;
        @Column("publisher")
        private PublisherRecord publisher;
    }

    @Getter
    @Setter
    public static class MixedBean {
        private String id;
        @ObjectColumn(prefix = "name")
        private PublisherNameRecord name_;
    }


    public record BookRecord(String id, @Column("titel") String title, @ObjectColumn(prefix = "p") PagesRecord seiten,
                             @ObjectColumn(prefix = "publisher") PublisherRecord publisher) {
    }

    public record PagesRecord(String pages) {
    }

    public record PublisherRecord(String id, @ObjectColumn(prefix = "name") PublisherNameRecord name_) {
    }

    public record PublisherNameRecord(@Column("name") String name_) {
    }

    public record IncorrectlyDeclaredBookRecord(String id, @Column("publisher") PublisherRecord publisher) {
    }

    public record MixedRecord(String id, @ObjectColumn(prefix = "p") Pages seiten) {

    }

    public enum Type {
        A
    }

}
