package de.sollder1.dbutils.core.query.exceptions;

/**
 * THsi Exception may be used to signal a user of a query-Method that
 * unexpectedly more than one result was returned from the database.
 */
public class MultiValueResultException extends RuntimeException {

    public MultiValueResultException(String message) {
        super(message);
    }

}
