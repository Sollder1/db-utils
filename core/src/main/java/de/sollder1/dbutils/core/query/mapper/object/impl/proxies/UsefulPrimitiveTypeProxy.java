package de.sollder1.dbutils.core.query.mapper.object.impl.proxies;

import de.sollder1.dbutils.core.query.mapper.object.spi.ValueProxy;

import java.sql.ResultSet;
import java.sql.SQLException;

@Deprecated(forRemoval = true)
public class UsefulPrimitiveTypeProxy implements ValueProxy {

    @Override
    public Object proxy(ResultSet resultSet, String name, Class<?> type) throws SQLException {
        if (type == Integer.TYPE) {
            return resultSet.getInt(name);
        } else if (type == Boolean.TYPE) {
            return resultSet.getBoolean(name);
        } else if (type == Double.TYPE) {
            return resultSet.getDouble(name);
        } else if (type == Long.TYPE) {
            return resultSet.getLong(name);
        }
        throw new SQLException("Unsupported type: " + type);
    }

    @Override
    public boolean canProxy(Class<?> from) {
        return from == Integer.TYPE || from == Long.TYPE || from == Double.TYPE || from == Boolean.TYPE;
    }

    @Override
    public int priority() {
        return 0;
    }
}
