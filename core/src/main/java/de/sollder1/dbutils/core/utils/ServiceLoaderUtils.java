package de.sollder1.dbutils.core.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;
import java.util.stream.Collectors;

public final class ServiceLoaderUtils {

    private ServiceLoaderUtils() {
        throw new UnsupportedOperationException();
    }

    public static <T> List<T> loadImplementationsJakartaEESafe(Class<T> clazz) {
        return ServiceLoader.load(clazz, ServiceLoaderUtils.class.getClassLoader()).stream()
                .map(ServiceLoader.Provider::get)
                .collect(Collectors.toCollection(ArrayList::new));
    }
}
