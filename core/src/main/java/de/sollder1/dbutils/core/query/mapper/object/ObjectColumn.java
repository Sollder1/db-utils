package de.sollder1.dbutils.core.query.mapper.object;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defines an Attribute as an ObjectColumn, meaning it should be mapped recursively.
 * Properties from the sql can be mapped by describing the tree-path.
 * Example for:
 * <pre>
 * {@code
 *  record A(@ObjectColumn(prefix = "b") B b) {
 *  }
 *
 *  record B(String id) {
 *  }
 * }
 * </pre>
 * <p>
 * One could write the following sql:
 *
 * <pre>{@code SELECT id AS "b.id" FROM example;}</pre>
 * <p>
 * The segments of the path are seperated by a dot '.'.
 * One may only use records OR beans in the whole tree.
 */
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface ObjectColumn {

    String prefix();

}
