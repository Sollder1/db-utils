package de.sollder1.dbutils.core.query.mapper;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * The same as GenericArrayMapper, but with a default java collection.
 */
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class GenericListMapper implements RowResultMapper<List<Object>> {

    public static GenericListMapper INSTANCE = new GenericListMapper();

    @Override
    public List<Object> map(ResultSet rowSet) throws SQLException {
        ResultSetMetaData metaData = rowSet.getMetaData();
        List<Object> data = new ArrayList<>();
        for (int i = 1; i <= metaData.getColumnCount(); i++) {
            data.add(rowSet.getObject(i));
        }

        return data;
    }
}
