package de.sollder1.dbutils.core.query.select;

import de.sollder1.dbutils.core.JdbcUtils;
import de.sollder1.dbutils.core.query.mapper.RowResultMapper;
import de.sollder1.dbutils.core.query.mapper.object.ObjectMapper;
import de.sollder1.dbutils.core.query.mapper.object.SingularColumnMapper;
import lombok.Builder;
import lombok.Singular;
import org.intellij.lang.annotations.Language;

import java.util.List;
import java.util.Optional;

/**
 * Classic select-query where parameters are passed in the right order.
 */
@Builder(builderClassName = "Builder")
public record IndexedSelectQuery<T>(String sql, RowResultMapper<T> mapper,
                                    @Singular List<Object> params) implements SelectQuery<T> {

    public IndexedSelectQuery(@Language("SQL") String sql, RowResultMapper<T> mapper, Object... params) {
        this(sql, mapper, List.of(params));
    }

    public static <T> Builder<T> builder(Class<T> clazz) {
        return new Builder<T>()
                .mapper(new ObjectMapper<>(clazz));
    }

    public static <T> Builder<T> builderSingleColumn(Class<T> clazz) {
        return new Builder<T>()
                .mapper(new SingularColumnMapper<>(clazz));
    }

    public static <T> Builder<T> builder(RowResultMapper<T> mapper) {
        return new Builder<T>()
                .mapper(mapper);
    }

    @Override
    public String provideSql() {
        return sql;
    }

    @Override
    public List<Object> provideParams() {
        return params;
    }

    public static class Builder<T> {

        public Builder<T> putParams(Object... params) {
            for (Object param : params) {
                param(param);
            }
            return this;
        }

        public Builder<T> sql(@Language("SQL") String sql) {
            this.sql = sql;
            return this;
        }

        public List<T> query(JdbcUtils jdbcUtils) {
            return jdbcUtils.query(build());
        }

        public Optional<T> queryOptional(JdbcUtils jdbcUtils) {
            return jdbcUtils.queryOptional(build());
        }

        public T querySingular(JdbcUtils jdbcUtils) {
            return jdbcUtils.querySingular(build());
        }

    }

}
