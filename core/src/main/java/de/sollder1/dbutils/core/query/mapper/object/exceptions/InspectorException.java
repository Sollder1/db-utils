package de.sollder1.dbutils.core.query.mapper.object.exceptions;

public class InspectorException extends RuntimeException {
    public InspectorException(String message) {
        super(message);
    }
}
