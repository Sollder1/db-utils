package de.sollder1.dbutils.core.query.types.impl.getter;

import de.sollder1.dbutils.core.query.types.spi.CustomGetTransformer;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GetEnumAsString implements CustomGetTransformer {

    @Override
    public Object get(ResultSet resultSet, String name, Class<?> type) throws SQLException {
        String enumAsString = resultSet.getString(name);
        return internal(type, enumAsString);
    }

    @Override
    public Object get(ResultSet resultSet, int index, Class<?> type) throws SQLException {
        String enumAsString = resultSet.getString(index);
        return internal(type, enumAsString);
    }

    @SuppressWarnings({"unchecked", "raw"})
    private static Enum<?> internal(Class<?> type, String enumAsString) {
        if (enumAsString == null) {
            return null;
        }

        return Enum.valueOf((Class<? extends Enum>) type, enumAsString);
    }


    @Override
    public boolean appliesTo(Class<?> type) {
        return type.isEnum();
    }

    @Override
    public int priority() {
        return 0;
    }
}
