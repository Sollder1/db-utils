package de.sollder1.dbutils.core.query.mapper.object.impl;

import de.sollder1.dbutils.core.query.mapper.object.Column;
import de.sollder1.dbutils.core.query.mapper.object.spi.CustomExpectedColumnNameResolver;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Optional;

public class SelfDefinedColumnAnnotationExpectedNameResolver implements CustomExpectedColumnNameResolver {

    @Override
    public Optional<String> determineColumnName(Method getter, Method setter, Field field) {
        Column fieldAnnotation = field.getAnnotation(Column.class);
        if (fieldAnnotation != null) {
            return fieldAnnotation.value().describeConstable();
        }

        Column setterAnnotation = setter.getAnnotation(Column.class);
        if (setterAnnotation != null) {
            return setterAnnotation.value().describeConstable();
        }

        Column getterAnnotation = getter.getAnnotation(Column.class);
        if (getterAnnotation != null) {
            return getterAnnotation.value().describeConstable();
        }
        return Optional.empty();
    }

    @Override
    public Optional<String> determineColumnName(Parameter parameter) {

        Column annotation = parameter.getAnnotation(Column.class);
        if (annotation != null) {
            return annotation.value().describeConstable();
        }

        return Optional.empty();
    }

    @Override
    public int priority() {
        return 10;
    }

    @Override
    public Class<? extends Annotation> getAnnotationType() {
        return Column.class;
    }


}
