package de.sollder1.dbutils.core.query.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Defines the generell API for Result-Mappers.
 * We should already have most needs met, specifically with {@link de.sollder1.dbutils.core.query.mapper.object.ObjectMapper},
 * {@link GenericArrayMapper} and {@link GenericMapMapper}.
 * <p>
 * But of course in special cases you may implement new ones as seen fit.
 *
 * @param <T> The Result-Type (per row)
 */
public interface RowResultMapper<T> {

    default void preValidate(ResultSet resultSet) throws SQLException {
        //NOOP
    }


    /**
     * <b>WARNING: DO NOT move the current pointer forward!</b>
     * <p>
     * Maps the Data of the ResultSet to an Instance of T.
     *
     * @param rowSet - The resultset currently beeing processed. Should be seen as readonly.
     * @return The mapped value
     * @throws SQLException If something with the jdbc api goes wrong.
     */
    T map(final ResultSet rowSet) throws SQLException;

}
