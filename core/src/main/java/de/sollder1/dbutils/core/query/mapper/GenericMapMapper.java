package de.sollder1.dbutils.core.query.mapper;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Returns a very basic representation of the Data with the default Types of the driver for the database fields.
 * The KEys of the Map are the labels given in the sql-Command.
 */
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class GenericMapMapper implements RowResultMapper<Map<String, Object>> {
    public static GenericMapMapper INSTANCE = new GenericMapMapper();

    @Override
    public Map<String, Object> map(ResultSet rowSet) throws SQLException {
        ResultSetMetaData metaData = rowSet.getMetaData();
        Map<String, Object> data = new HashMap<>();
        for (int i = 1; i <= metaData.getColumnCount(); i++) {
            data.put(metaData.getColumnName(i), rowSet.getObject(i));
        }
        return data;
    }

}
