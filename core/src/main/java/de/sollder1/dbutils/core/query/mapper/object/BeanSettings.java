package de.sollder1.dbutils.core.query.mapper.object;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Allows certain settigns to be applied on BEans (Not records!).
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface BeanSettings {

    /**
     * @return If true only fields which are annotated with an Annotations supported by a
     * registered CustomExpectedColumnNameResolver are mapped.
     */
    boolean onlyMapAnnotatedFields() default false;

}
