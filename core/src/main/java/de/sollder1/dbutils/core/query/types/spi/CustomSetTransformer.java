package de.sollder1.dbutils.core.query.types.spi;

import java.sql.PreparedStatement;
import java.sql.SQLException;


public non-sealed interface CustomSetTransformer extends CustomTransformer {

    void set(PreparedStatement resultSet, String name, Object value) throws SQLException;

    void set(PreparedStatement resultSet, int index, Object value) throws SQLException;

}
