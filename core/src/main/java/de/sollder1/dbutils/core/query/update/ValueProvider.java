package de.sollder1.dbutils.core.query.update;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Singular;
import org.intellij.lang.annotations.Language;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

@Getter
@Builder
@AllArgsConstructor
public class ValueProvider<T> {

    public static Mapper<Object[]> OBJ_ARRAY_MAPPER = ValueProvider::objectArrayMapper;

    private static void objectArrayMapper(Object[] objects, PreparedStatement preparedStatement) throws SQLException {
        for (int i = 0; i < objects.length; i++) {
            preparedStatement.setObject(i + 1, objects[i]);
        }
    }

    @Singular
    private final List<T> values;
    @Language("SQL")
    private final String sql;
    @Builder.Default
    private final int batchSize = Integer.MAX_VALUE;
    private Mapper<T> mapper;
    private Class<T> type;

    public static <T> ValueProviderBuilder<T> builder(Class<T> type) {
        return new ValueProviderBuilder<T>()
                .type(type);
    }

    public interface Mapper<DATA> {
        void map(DATA data, PreparedStatement preparedStatement) throws SQLException;
    }

    public static class ValueProviderBuilder<T> {

        public ValueProviderBuilder<T> sql(@Language("SQL") String sql) {
            this.sql = sql;
            return this;
        }

    }


}
