package de.sollder1.dbutils.core.query.types.spi;

import java.sql.ResultSet;
import java.sql.SQLException;

public non-sealed interface CustomGetTransformer extends CustomTransformer {

    Object get(ResultSet resultSet, String name, Class<?> type) throws SQLException;

    Object get(ResultSet resultSet, int index, Class<?> type) throws SQLException;

}
