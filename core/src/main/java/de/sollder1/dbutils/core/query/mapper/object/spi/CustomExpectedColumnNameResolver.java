package de.sollder1.dbutils.core.query.mapper.object.spi;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Optional;

/**
 * Portable and extendable way to implement a Column-Name Provider Annotation Resolver.
 * See META-INF/services for an example on how to register implementations.
 */
public interface CustomExpectedColumnNameResolver {

    /**
     * Scans the passed arguments for Annotations that indicate expected Column-Label.
     *
     * @param field  The field to scan for annotations, has the highest priority.
     * @param setter The setter to scan for annotations, has medium priority.
     * @param getter The getter to scan for annotations, has the lowest priority.
     * @return The expected Column-Label for the field.
     */
    Optional<String> determineColumnName(Method getter, Method setter, Field field);

    /**
     * Scans the passed arguments for Annotations that indicate expected Column-Label.
     *
     * @param parameter The parameter to scan for annotations.
     * @return The expected Column-Label for the field.
     */
    Optional<String> determineColumnName(Parameter parameter);

    /**
     * @return The priority, the higher the value, the earlier this implementation is called.
     * If this implementation provides a value no downstream implementations will be called.
     */
    int priority();

    /**
     * @return The Type of the decoration Annotation, useful for strict-mode.
     */
    Class<? extends Annotation> getAnnotationType();
}
