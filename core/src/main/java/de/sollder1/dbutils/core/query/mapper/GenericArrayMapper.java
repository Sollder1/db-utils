package de.sollder1.dbutils.core.query.mapper;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

/**
 * Returns a very basic representation of the Data with the default Types of the driver for the database fields.
 * The order will be the same as defined in the sql-Command.
 */
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class GenericArrayMapper implements RowResultMapper<Object[]> {

    public static GenericArrayMapper INSTANCE = new GenericArrayMapper();

    @Override
    public Object[] map(ResultSet rowSet) throws SQLException {
        ResultSetMetaData metaData = rowSet.getMetaData();
        Object[] data = new Object[metaData.getColumnCount()];
        for (int i = 1; i <= metaData.getColumnCount(); i++) {
            data[i - 1] = rowSet.getObject(i);
        }

        return data;
    }
}
