package de.sollder1.dbutils.core.query.mapper.object.spi;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Portable way to register an ValueProxy with the application.
 * A ValueProxy can intercept the call to a {@link ResultSet} and run custom logic to create an instance of the given type.
 * For an Example look at {@link de.sollder1.dbutils.core.query.mapper.object.impl.proxies.EnumAsStringProxy} which
 * implements support for all enums.
 * <p>
 * Implementations can be loaded with the ServiceLoader System.
 *
 * @deprecated See {@link de.sollder1.dbutils.core.query.types.spi.CustomGetTransformer} for an equivalent replacement.
 */
@Deprecated(forRemoval = true)
public interface ValueProxy {

    /**
     * Run your custom logic instead of the default <code>resultSet.getObject(name, type);</code>
     *
     * @param resultSet The resultSet that holds the data, DO NOT put the pointer forward.
     * @param name      The Name of the property we want to load
     * @param type      The Type of the property we expect Object to be.
     * @return A value of the given type or null
     * @throws SQLException If something goes wrong
     */
    Object proxy(ResultSet resultSet, String name, Class<?> type) throws SQLException;

    /**
     * Returns if this proxy can handle the given type.
     *
     * @param from The type to proxy
     * @return If proxying is possible
     */
    boolean canProxy(Class<?> from);

    /**
     * @return The priority of execution. If there are two ValueProxy who can handle a type T the
     * one with the higher priority wins. Behaviour with two equivalent priority-values is undefined.
     */
    int priority();

}
