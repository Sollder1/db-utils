package de.sollder1.dbutils.core.query.mapper.object;

import de.sollder1.dbutils.core.query.mapper.object.exceptions.InspectorException;
import de.sollder1.dbutils.core.query.mapper.object.exceptions.ReflectiveOperationWrapperException;
import de.sollder1.dbutils.core.query.mapper.object.spi.CustomExpectedColumnNameResolver;
import de.sollder1.dbutils.core.utils.ServiceLoaderUtils;
import lombok.Getter;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;


public final class ObjectInspector {

    @Getter
    private static final List<CustomExpectedColumnNameResolver> columnNameResolver;
    private static final Set<Class<? extends Annotation>> DECORATING_ANNOTATIONS;


    private static final Map<Class<?>, Map<String, BeanEntry>> CACHE = new ConcurrentHashMap<>();
    private static final Map<Class<?>, List<RecordEntry>> RECORD_CACHE = new ConcurrentHashMap<>();


    static {
        var temp = ServiceLoaderUtils.loadImplementationsJakartaEESafe(CustomExpectedColumnNameResolver.class);
        temp.sort(Comparator.comparing(CustomExpectedColumnNameResolver::priority).reversed());
        columnNameResolver = List.copyOf(temp);
        DECORATING_ANNOTATIONS = Set.copyOf(columnNameResolver.stream()
                .map(CustomExpectedColumnNameResolver::getAnnotationType)
                .collect(Collectors.toSet()));
    }


    private ObjectInspector() {
        throw new UnsupportedOperationException();
    }


    static List<RecordEntry> getOrBuildForRecordClass(Class<?> type) {
        if (RECORD_CACHE.containsKey(type)) {
            return RECORD_CACHE.get(type);
        }

        synchronized (ObjectInspector.class) {
            if (RECORD_CACHE.containsKey(type)) {
                return RECORD_CACHE.get(type);
            }
            var value = List.copyOf(buildRecord(type, ""));
            RECORD_CACHE.put(type, value);
            return value;
        }
    }

    private static List<RecordEntry> buildRecord(Class<?> type, String prefix) {
        if (type.getDeclaredConstructors().length != 1) {
            throw new InspectorException("Records must have exactly one constructor!");
        }

        List<RecordEntry> list = new ArrayList<>();
        Parameter[] parameters = type.getDeclaredConstructors()[0].getParameters();
        for (int i = 0; i < parameters.length; i++) {
            final Parameter parameter = parameters[i];

            if (parameter.isAnnotationPresent(ObjectColumn.class)) {
                if (!parameter.getType().isRecord()) {
                    throw new InspectorException("Records must only have record ObjectColumn parameters!");
                }

                String localPrefixPart = parameter.getAnnotation(ObjectColumn.class).prefix();
                String newPrefix = appendToPrefix(prefix, localPrefixPart);
                list.add(new ComplexRecordEntry(parameter, i, newPrefix, buildRecord(parameter.getType(), newPrefix)));
            } else {
                String name = determineColumnName(parameter);
                list.add(new SimpleRecordEntry(parameter, i, appendToPrefix(prefix, name)));
            }
        }

        return list;
    }


    private static String determineColumnName(Parameter parameter) {
        for (var resolver : columnNameResolver) {
            Optional<String> name = resolver.determineColumnName(parameter);
            if (name.isPresent()) {
                return name.get();
            }
        }

        return parameter.getName();
    }


    static Map<String, BeanEntry> getOrBuildForBeanClass(Class<?> type) {
        if (CACHE.containsKey(type)) {
            return CACHE.get(type);
        }

        synchronized (ObjectInspector.class) {
            if (CACHE.containsKey(type)) {
                return CACHE.get(type);
            }
            Map<String, BeanEntry> value = Map.copyOf(buildBean(type, ""));
            CACHE.put(type, value);
            return value;
        }
    }

    private static Map<String, BeanEntry> buildBean(Class<?> type, String prefix) {

        final boolean onlyMapAnnotatedFields = type.isAnnotationPresent(BeanSettings.class) &&
                                               type.getAnnotation(BeanSettings.class).onlyMapAnnotatedFields();
        Class<?> typeIterator = type;

        List<BeanEntry> entries = new ArrayList<>();

        while (!Object.class.equals(typeIterator)) {
            Arrays.stream(typeIterator.getDeclaredFields())
                    .filter(field -> !java.lang.reflect.Modifier.isStatic(field.getModifiers()) &&
                                     !java.lang.reflect.Modifier.isFinal(field.getModifiers()))
                    .filter(field -> !onlyMapAnnotatedFields || hasStrictModeAnnotation(field))
                    .map(field -> toBeanEntry(type, field, prefix))
                    .forEach(entries::add);
            typeIterator = typeIterator.getSuperclass();
        }

        return entries.stream()
                .collect(Collectors.toMap(BeanEntry::name, Function.identity()));
    }

    private static boolean hasStrictModeAnnotation(Field field) {

        for (var decoratingAnnotation : DECORATING_ANNOTATIONS) {
            if (field.isAnnotationPresent(decoratingAnnotation)) {
                return true;
            }
        }

        return field.isAnnotationPresent(ObjectColumn.class);
    }

    private static BeanEntry toBeanEntry(Class<?> type, Field field, String prefix) {
        try {
            Method getter = findGetter(type, field);
            Method setter = findSetter(type, field);

            if (!field.isAnnotationPresent(ObjectColumn.class)) {
                return new DirectBeanEntry(field, getter, setter, appendToPrefix(prefix, determineColumnName(getter, setter, field)));
            }

            //Hint: Not really complete Check...
            if (field.getType().isRecord()) {
                throw new InspectorException("ObjectColumn Elements whose root is a Java-Bean must also be Java-Beans!");
            }

            String localPrefixPart = field.getAnnotation(ObjectColumn.class).prefix();
            String newPrefix = appendToPrefix(prefix, localPrefixPart);
            return new ComplexBeanEntry(field, getter, setter, newPrefix, buildBean(field.getType(), newPrefix));

        } catch (ReflectiveOperationException e) {
            throw new ReflectiveOperationWrapperException("toBeanEntry", e);
        }
    }

    private static String appendToPrefix(String prefix, String localPrefixPart) {
        return prefix.isEmpty() ? localPrefixPart : prefix + prefixSeparator() + localPrefixPart;
    }

    private static String prefixSeparator() {
        return ".";
    }

    private static String determineColumnName(Method getter, Method setter, Field field) {

        for (var resolver : columnNameResolver) {
            Optional<String> name = resolver.determineColumnName(getter, setter, field);
            if (name.isPresent()) {
                return name.get();
            }
        }

        return field.getName();
    }

    private static Method findSetter(Class<?> type, Field field) throws ReflectiveOperationException {
        return type.getMethod("set" + toPropertyName(field), field.getType());
    }

    private static Method findGetter(Class<?> type, Field field) throws ReflectiveOperationException {
        String prefix = "get";

        if (Boolean.TYPE.equals(field.getType())) {
            prefix = "is";
        }

        return type.getMethod(prefix + toPropertyName(field));
    }

    private static String toPropertyName(Field field) {
        String fieldName = field.getName();
        return fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
    }


    sealed interface BeanEntry permits DirectBeanEntry, ComplexBeanEntry {
        String name();

        Field field();

        Method getter();

        Method setter();

    }

    record DirectBeanEntry(Field field, Method getter, Method setter,
                           String expectedColumnName) implements BeanEntry {

        @Override
        public String name() {
            return expectedColumnName;
        }
    }

    record ComplexBeanEntry(Field field, Method getter, Method setter, String propertyPrefix,
                            Map<String, BeanEntry> submodells) implements BeanEntry {

        @Override
        public String name() {
            return propertyPrefix;
        }
    }

    sealed interface RecordEntry permits ComplexRecordEntry, SimpleRecordEntry {
        Parameter param();

        int position();

        String name();
    }


    record SimpleRecordEntry(Parameter param, int position, String expectedColumnName) implements RecordEntry {

        @Override
        public String name() {
            return expectedColumnName;
        }
    }

    record ComplexRecordEntry(Parameter param, int position, String propertyPrefix,
                              List<RecordEntry> submodels) implements RecordEntry {

        @Override
        public String name() {
            return propertyPrefix;
        }
    }


}
