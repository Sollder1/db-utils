package de.sollder1.dbutils.core.query.mapper.object.impl.proxies;

import de.sollder1.dbutils.core.query.mapper.object.spi.ValueProxy;

import java.sql.ResultSet;
import java.sql.SQLException;

@Deprecated(forRemoval = true)
public class EnumAsStringProxy implements ValueProxy {

    @Override
    public Object proxy(ResultSet resultSet, String name, Class<?> type) throws SQLException {
        String enumAsString = resultSet.getString(name);
        if (enumAsString == null) {
            return null;
        }

        return Enum.valueOf((Class<? extends Enum>) type, enumAsString);
    }

    @Override
    public boolean canProxy(Class<?> from) {
        return from.isEnum();
    }

    @Override
    public int priority() {
        return 0;
    }
}
