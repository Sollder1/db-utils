package de.sollder1.dbutils.core.query.mapper.object;

import de.sollder1.dbutils.core.query.mapper.RowResultMapper;
import de.sollder1.dbutils.core.query.types.TypeTransformationManager;
import lombok.AllArgsConstructor;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * This Mapper allows for easier Mapping of results that have only one column.
 *
 * @param <T> Type of the Column to map.
 */
@AllArgsConstructor
public class SingularColumnMapper<T> implements RowResultMapper<T> {

    private final Class<T> columnType;

    @Override
    public void preValidate(ResultSet resultSet) throws SQLException {
        if (resultSet.getMetaData().getColumnCount() != 1) {
            throw new SQLException("Single value mapper expects a single column!");
        }
    }

    @Override
    public T map(ResultSet resultSet) throws SQLException {
        return TypeTransformationManager.get(resultSet, resultSet.getMetaData().getColumnLabel(1), columnType);
    }
}
