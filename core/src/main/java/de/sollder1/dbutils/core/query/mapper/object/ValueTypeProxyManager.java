package de.sollder1.dbutils.core.query.mapper.object;

import de.sollder1.dbutils.core.query.mapper.object.spi.ValueProxy;
import de.sollder1.dbutils.core.utils.ServiceLoaderUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

/**
 * @deprecated See {@link de.sollder1.dbutils.core.query.types.TypeTransformationManager} for an equivalent replacement.
 */
@Deprecated(forRemoval = true)
public class ValueTypeProxyManager {

    private static final List<ValueProxy> proxies;

    static {
        var temp = ServiceLoaderUtils.loadImplementationsJakartaEESafe(ValueProxy.class);
        temp.sort(Comparator.comparing(ValueProxy::priority).reversed());
        proxies = List.copyOf(temp);
    }

    public static <T> T convert(ResultSet resultSet, String name, Class<T> type) throws SQLException {

        for (ValueProxy proxy : proxies) {
            if (proxy.canProxy(type)) {
                return (T) proxy.proxy(resultSet, name, type);
            }
        }

        return resultSet.getObject(name, type);
    }


}
