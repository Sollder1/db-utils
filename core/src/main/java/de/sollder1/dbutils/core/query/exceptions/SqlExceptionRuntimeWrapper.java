package de.sollder1.dbutils.core.query.exceptions;

import java.sql.SQLException;

/**
 * Used to wrap SQLException`s, which are checked, into a RuntimeException.
 */
public class SqlExceptionRuntimeWrapper extends RuntimeException {

    public SqlExceptionRuntimeWrapper(SQLException cause) {
        super(cause);
    }

    @Override
    public synchronized SQLException getCause() {
        return (SQLException) super.getCause();
    }
}
