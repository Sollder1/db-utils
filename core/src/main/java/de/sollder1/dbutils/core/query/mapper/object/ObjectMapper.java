package de.sollder1.dbutils.core.query.mapper.object;

import de.sollder1.dbutils.core.query.mapper.RowResultMapper;
import de.sollder1.dbutils.core.query.mapper.object.exceptions.ReflectiveOperationWrapperException;
import de.sollder1.dbutils.core.query.types.TypeTransformationManager;
import lombok.AllArgsConstructor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
public class ObjectMapper<T> implements RowResultMapper<T> {

    private final Class<T> type;

    @Override
    public T map(ResultSet rowSet) throws SQLException {
        try {
            return mapInternal(rowSet);
        } catch (ReflectiveOperationException e) {
            throw new ReflectiveOperationWrapperException("map", e);
        }
    }

    private T mapInternal(ResultSet rowSet) throws ReflectiveOperationException, SQLException {
        if (type.isRecord()) {
            return handleRecordType(rowSet);
        } else {
            return handleNormalType(rowSet);
        }
    }

    private T handleRecordType(ResultSet rowSet) throws ReflectiveOperationException, SQLException {
        final var cachedModel = ObjectInspector.getOrBuildForRecordClass(type);
        return handleRecordType(rowSet, type, cachedModel);
    }

    private <L> L handleRecordType(ResultSet rowSet, Class<L> type, List<ObjectInspector.RecordEntry> recordEntries) throws ReflectiveOperationException, SQLException {
        Object[] dataToPass = new Object[recordEntries.size()];
        for (int i = 0; i < recordEntries.size(); i++) {
            var recordEntry = recordEntries.get(i);
            switch (recordEntry) {
                case ObjectInspector.SimpleRecordEntry e -> {
                    Object value = TypeTransformationManager.get(rowSet, e.expectedColumnName(), e.param().getType());
                    dataToPass[i] = value;
                }
                case ObjectInspector.ComplexRecordEntry e ->
                        dataToPass[i] = handleRecordType(rowSet, e.param().getType(), e.submodels());
            }
        }

        return (L) type.getDeclaredConstructors()[0].newInstance(dataToPass);
    }


    private T handleNormalType(ResultSet rowSet) throws ReflectiveOperationException, SQLException {
        final var cachedModel = ObjectInspector.getOrBuildForBeanClass(type);
        return handleNormalType(rowSet, type, cachedModel);
    }

    private <L> L handleNormalType(ResultSet rowSet, Class<L> type, Map<String, ObjectInspector.BeanEntry> model) throws ReflectiveOperationException, SQLException {

        final L instance = type.getDeclaredConstructor().newInstance();
        for (var fieldModel : model.values()) {
            switch (fieldModel) {
                case ObjectInspector.ComplexBeanEntry e ->
                        e.setter().invoke(instance, handleNormalType(rowSet, e.field().getType(), e.submodells()));
                case ObjectInspector.DirectBeanEntry e -> {
                    Object value = TypeTransformationManager.get(rowSet, e.expectedColumnName(), fieldModel.field().getType());
                    fieldModel.setter().invoke(instance, value);
                }
            }
        }
        return instance;
    }


}
