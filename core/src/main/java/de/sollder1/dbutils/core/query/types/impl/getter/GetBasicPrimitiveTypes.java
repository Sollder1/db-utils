package de.sollder1.dbutils.core.query.types.impl.getter;

import de.sollder1.dbutils.core.query.types.spi.CustomGetTransformer;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GetBasicPrimitiveTypes implements CustomGetTransformer {

    @Override
    public Object get(ResultSet resultSet, String name, Class<?> type) throws SQLException {
        if (type == Integer.TYPE) {
            return resultSet.getInt(name);
        } else if (type == Boolean.TYPE) {
            return resultSet.getBoolean(name);
        } else if (type == Double.TYPE) {
            return resultSet.getDouble(name);
        } else if (type == Long.TYPE) {
            return resultSet.getLong(name);
        }
        throw new SQLException("Unsupported type: " + type);
    }

    @Override
    public Object get(ResultSet resultSet, int index, Class<?> type) throws SQLException {
        if (type == Integer.TYPE) {
            return resultSet.getInt(index);
        } else if (type == Boolean.TYPE) {
            return resultSet.getBoolean(index);
        } else if (type == Double.TYPE) {
            return resultSet.getDouble(index);
        } else if (type == Long.TYPE) {
            return resultSet.getLong(index);
        }
        throw new SQLException("Unsupported type: " + type);
    }

    @Override
    public boolean appliesTo(Class<?> type) {
        return type == Integer.TYPE || type == Long.TYPE || type == Double.TYPE || type == Boolean.TYPE;
    }

    @Override
    public int priority() {
        return 0;
    }
}
