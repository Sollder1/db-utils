package de.sollder1.dbutils.core.query.types.spi;

public sealed interface CustomTransformer permits CustomGetTransformer, CustomSetTransformer {

    boolean appliesTo(Class<?> type);

    /**
     * @return The priority of execution. If there are two ValueProxy who can handle a type T the
     * one with the higher priority wins. Behaviour with two equivalent priority-values is undefined.
     */
    int priority();
}
