package de.sollder1.dbutils.core;

import de.sollder1.dbutils.core.query.exceptions.MultiValueResultException;
import de.sollder1.dbutils.core.query.exceptions.SqlExceptionRuntimeWrapper;
import de.sollder1.dbutils.core.query.select.SelectQuery;
import de.sollder1.dbutils.core.query.types.TypeTransformationManager;
import de.sollder1.dbutils.core.query.update.ValueProvider;
import lombok.Getter;
import org.intellij.lang.annotations.Language;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * The Central API to interact with this libery.
 * Extensions like the jakarta-extension may define platform-specific ways tro obtain an instance of this class.
 * For the purposes of JavaSE one can simply create and manage his own instances of this class.
 * <p>
 * It is generally advisable AND safe to have one Instance per Datasource, whereas a {@link DataSource} represents
 * the App-Global connection to exactly one Database.
 */
public class JdbcUtils {

    @Getter
    private final DataSource dataSource;

    public JdbcUtils(final DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public <T> List<T> query(SelectQuery<T> selectQuery) throws SqlExceptionRuntimeWrapper {
        try (var connection = dataSource.getConnection();
             var preparedStatement = connection.prepareStatement(selectQuery.provideSql())) {
            List<T> results = new ArrayList<>();
            for (int i = 0; i < selectQuery.provideParams().size(); i++) {
                preparedStatement.setObject(i + 1, selectQuery.provideParams().get(i));
            }
            try (var resultSet = preparedStatement.executeQuery()) {
                selectQuery.mapper().preValidate(resultSet);
                while (resultSet.next()) {
                    T value = selectQuery.mapper().map(resultSet);
                    results.add(value);
                }
            }
            return results;
        } catch (SQLException e) {
            throw new SqlExceptionRuntimeWrapper(e);
        }
    }

    public <T> T querySingular(SelectQuery<T> selectQuery) throws SqlExceptionRuntimeWrapper {
        List<T> result = query(selectQuery);
        if (result.size() != 1) {
            throw new MultiValueResultException("Expected exactly one result, got %s".formatted(result.size()));
        }
        return result.getFirst();
    }

    public <T> Optional<T> queryOptional(SelectQuery<T> selectQuery) throws SqlExceptionRuntimeWrapper {
        List<T> result = query(selectQuery);

        if (result.isEmpty()) {
            return Optional.empty();
        }
        if (result.size() > 1) {
            throw new MultiValueResultException("Expected one or zero result, got %s".formatted(result.size()));
        }
        return Optional.of(result.getFirst());
    }

    public int runUpdate(@Language("SQL") String sql, Object... params) throws SqlExceptionRuntimeWrapper {
        try (var connection = dataSource.getConnection();
             var preparedStatement = connection.prepareStatement(sql)) {
            for (int i = 0; i < params.length; i++) {
                TypeTransformationManager.set(preparedStatement, i + 1, params[i]);
            }
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new SqlExceptionRuntimeWrapper(e);
        }
    }

    public <T> int[] runUpdate(ValueProvider<T> valueProvider) throws SqlExceptionRuntimeWrapper {
        try (var connection = dataSource.getConnection();
             var preparedStatement = connection.prepareStatement(valueProvider.getSql())) {
            List<T> values = valueProvider.getValues();
            for (T value : values) {
                valueProvider.getMapper().map(value, preparedStatement);
                preparedStatement.addBatch();
            }
            return preparedStatement.executeBatch();
        } catch (SQLException e) {
            throw new SqlExceptionRuntimeWrapper(e);
        }
    }

    public boolean execute(@Language("SQL") String sql, Object... params) throws SqlExceptionRuntimeWrapper {
        try (var connection = dataSource.getConnection();
             var preparedStatement = connection.prepareStatement(sql)) {
            for (int i = 0; i < params.length; i++) {
                TypeTransformationManager.set(preparedStatement, i + 1, params[i]);
            }
            return preparedStatement.execute();
        } catch (SQLException e) {
            throw new SqlExceptionRuntimeWrapper(e);
        }
    }

}
