package de.sollder1.dbutils.core.query.types;

import de.sollder1.dbutils.core.query.mapper.object.ValueTypeProxyManager;
import de.sollder1.dbutils.core.query.types.spi.CustomGetTransformer;
import de.sollder1.dbutils.core.query.types.spi.CustomSetTransformer;
import de.sollder1.dbutils.core.query.types.spi.CustomTransformer;
import de.sollder1.dbutils.core.utils.ServiceLoaderUtils;

import java.math.BigDecimal;
import java.sql.*;
import java.util.Date;
import java.util.*;

public class TypeTransformationManager {

    private static final Set<Class<?>> shortCircuitTypes = new HashSet<>();

    //Hint: Taken from:
    // https://download.oracle.com/otn-pub/jcp/jdbc-4_3-mrel3-eval-spec/jdbc4.3-fr-spec.pdf?AuthParam=1719002610_8f59d35bed4a12777f6decab8f2e8bc1
    // APENDIX B Table 3

    static {
        shortCircuitTypes.add(String.class);
        shortCircuitTypes.add(Boolean.class);
        shortCircuitTypes.add(byte[].class);

        //Time:
        shortCircuitTypes.add(Date.class);
        shortCircuitTypes.add(Time.class);
        shortCircuitTypes.add(Timestamp.class);

        //Numbers:
        shortCircuitTypes.add(Integer.class);
        shortCircuitTypes.add(Long.class);
        shortCircuitTypes.add(Double.class);
        shortCircuitTypes.add(Float.class);
        shortCircuitTypes.add(BigDecimal.class);
    }


    private static final List<CustomGetTransformer> customGetter;
    private static final List<CustomSetTransformer> customSetter;

    static {
        customGetter = load(CustomGetTransformer.class);
        customSetter = load(CustomSetTransformer.class);
    }

    public static <T> void set(PreparedStatement resultSet, int index, T value) throws SQLException {

        if (value == null) {
            resultSet.setObject(index, value);
            return;
        }

        final var type = value.getClass();

        if (shortCircuitTypes.contains(type)) {
            resultSet.setObject(index, value);
            return;
        }

        for (var setter : customSetter) {
            if (setter.appliesTo(type)) {
                setter.set(resultSet, index, value);
                return;
            }
        }

        resultSet.setObject(index, value);
    }


    @SuppressWarnings({"unchecked", "deprecation"})
    public static <T> T get(ResultSet resultSet, String name, Class<T> type) throws SQLException {

        if (shortCircuitTypes.contains(type)) {
            return resultSet.getObject(name, type);
        }

        for (var getter : customGetter) {
            if (getter.appliesTo(type)) {
                return (T) getter.get(resultSet, name, type);
            }
        }

        //Ensure backward compatibility...
        return ValueTypeProxyManager.convert(resultSet, name, type);
    }


    private static <T extends CustomTransformer> List<T> load(Class<T> clazz) {
        List<T> temp = ServiceLoaderUtils.loadImplementationsJakartaEESafe(clazz);
        temp.sort(Comparator.comparing(CustomTransformer::priority).reversed());
        return List.copyOf(temp);
    }


}
