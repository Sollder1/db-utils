package de.sollder1.dbutils.core.query.mapper.object.impl.proxies;

import de.sollder1.dbutils.core.query.mapper.object.spi.ValueProxy;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Allows to Read an Array
 */
@Deprecated(forRemoval = true)
public class ReadArrayProxy implements ValueProxy {

    @Override
    public Object proxy(ResultSet resultSet, String name, Class<?> type) throws SQLException {
        return resultSet.getArray(name).getArray();
    }

    @Override
    public boolean canProxy(Class<?> from) {
        return from.isArray();
    }

    @Override
    public int priority() {
        return 0;
    }
}
