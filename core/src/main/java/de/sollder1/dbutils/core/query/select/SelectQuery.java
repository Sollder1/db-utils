package de.sollder1.dbutils.core.query.select;

import de.sollder1.dbutils.core.query.mapper.RowResultMapper;

import java.util.List;

/**
 * Describes a select-query. This API allows very flexible custom Implementations, should,
 * for some reason, the ones provided not suffice.
 *
 * @param <T> The Type produced by this Query.
 */
public interface SelectQuery<T> {

    /**
     * @return The sql to be executed against the database.
     */
    String provideSql();

    /**
     * @return The params to be used when executing the query as a prepared statement.
     * Beware, that order very much matters.
     */
    List<Object> provideParams();

    /**
     * @return The ResultMapper to use when mapping the ResultSet.
     */
    RowResultMapper<T> mapper();

}
