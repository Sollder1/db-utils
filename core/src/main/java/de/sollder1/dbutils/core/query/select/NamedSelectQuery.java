package de.sollder1.dbutils.core.query.select;

import de.sollder1.dbutils.core.JdbcUtils;
import de.sollder1.dbutils.core.query.mapper.RowResultMapper;
import de.sollder1.dbutils.core.query.mapper.object.ObjectMapper;
import de.sollder1.dbutils.core.query.mapper.object.SingularColumnMapper;
import lombok.Builder;
import lombok.Singular;
import org.intellij.lang.annotations.Language;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Implementations that allows for names to be used in prepared statement parameters.
 * Example: <code>SELECT * FROM example WHERE id = :id</code>. So the syntax for parameters is: ":name".
 * This follows the standard for named params in SQL, because of that IntelliJ and indeed possibly other IDEs can highlight the code correctly.
 * Attention: one may only use the characters [a-z]; [A-Z] and [_] for these param_names.
 */
@Builder(builderClassName = "Builder")
public record NamedSelectQuery<T>(@Language("SQL") String sql, RowResultMapper<T> mapper,
                                  @Singular Map<String, Object> params) implements SelectQuery<T> {

    public static final Pattern PATTERN = Pattern.compile(":[a-zA-Z_]+");

    public static <T> Builder<T> builder(Class<T> clazz) {
        return new Builder<T>()
                .mapper(new ObjectMapper<>(clazz));
    }

    public static <T> Builder<T> builderSingleColumn(Class<T> clazz) {
        return new Builder<T>()
                .mapper(new SingularColumnMapper<>(clazz));
    }

    public static <T> Builder<T> builder(RowResultMapper<T> mapper) {
        return new Builder<T>()
                .mapper(mapper);
    }

    @Override
    public String provideSql() {
        return PATTERN.matcher(sql).replaceAll("?");
    }

    @Override
    public List<Object> provideParams() {
        Matcher matcher = PATTERN.matcher(sql);

        List<String> names = new ArrayList<>();
        while (matcher.find()) {
            names.add(matcher.group().substring(1));
        }

        return names.stream()
                .map(this::findValue)
                .toList();
    }


    private Object findValue(String name) {
        if (!params.containsKey(name)) {
            throw new IllegalArgumentException("Parameter " + name + " not provided!");
        }

        return params.get(name);
    }

    public static class Builder<T> {

        public Builder<T> sql(@Language("SQL") String sql) {
            this.sql = sql;
            return this;
        }

        public List<T> query(JdbcUtils jdbcUtils) {
            return jdbcUtils.query(build());
        }

        public Optional<T> queryOptional(JdbcUtils jdbcUtils) {
            return jdbcUtils.queryOptional(build());
        }

        public T querySingular(JdbcUtils jdbcUtils) {
            return jdbcUtils.querySingular(build());
        }

    }

}
