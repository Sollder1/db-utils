package de.sollder1.dbutils.core.query.mapper.object.exceptions;

public class ReflectiveOperationWrapperException extends RuntimeException {

    public ReflectiveOperationWrapperException(String message, ReflectiveOperationException cause) {
        super(message, cause);
    }

    @Override
    public synchronized ReflectiveOperationException getCause() {
        return (ReflectiveOperationException) super.getCause();
    }
}
