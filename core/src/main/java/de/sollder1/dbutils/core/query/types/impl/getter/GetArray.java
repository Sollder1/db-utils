package de.sollder1.dbutils.core.query.types.impl.getter;

import de.sollder1.dbutils.core.query.types.spi.CustomGetTransformer;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GetArray implements CustomGetTransformer {

    @Override
    public Object get(ResultSet resultSet, String name, Class<?> type) throws SQLException {
        return resultSet.getArray(name).getArray();
    }

    @Override
    public Object get(ResultSet resultSet, int index, Class<?> type) throws SQLException {
        return resultSet.getArray(index).getArray();
    }

    @Override
    public boolean appliesTo(Class<?> type) {
        return type.isArray();
    }

    @Override
    public int priority() {
        return 0;
    }
}
